package com.js.totossapi.service;

import com.js.totossapi.entity.Member;
import com.js.totossapi.entity.Remittance;
import com.js.totossapi.model.remittance.RemittaceChangeRequest;
import com.js.totossapi.model.remittance.RemittanceItem;
import com.js.totossapi.model.remittance.RemittanceRequest;
import com.js.totossapi.model.remittance.RemittanceResponse;
import com.js.totossapi.resistory.RemittanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RemittanceService {
    private final RemittanceRepository remittanceRepository;

    public void setRemittance(RemittanceRequest request , Member member){
        Remittance addData = new Remittance();

        addData.setMemberId(member);
        addData.setDateTrade(LocalDate.now());
        addData.setDepositor(request.getDepositor());
        addData.setBankName(request.getBankName());
        addData.setAccountNumber(request.getAccountNumber());

        remittanceRepository.save(addData);

    }

    public List<RemittanceItem> getRemittances (){
        List<Remittance> originlist = remittanceRepository.findAll();
        List<RemittanceItem> result = new LinkedList<>();

        for(Remittance remittance : originlist){
            RemittanceItem addItem = new RemittanceItem();

            addItem.setId(remittance.getId());
            addItem.setMemberId(remittance.getMemberId().getId());
            addItem.setMemberName(remittance.getMemberId().getName());
            addItem.setDepositor(remittance.getDepositor());

            result.add(addItem);
        }
            return result;
    }
    public RemittanceResponse getRemittance(long id){
        Remittance originData = remittanceRepository.findById(id).orElseThrow();
        RemittanceResponse response = new RemittanceResponse();

        response.setId(originData.getId());
        response.setMemberId(originData.getMemberId().getId());
        response.setMemberName(originData.getMemberId().getName());
        response.setDepositor(originData.getDepositor());
        response.setAccountNumber(originData.getAccountNumber());

        return response;
    }

    public void putRemittance(long id , RemittaceChangeRequest request){
        Remittance originData = remittanceRepository.findById(id).orElseThrow();

        originData.setDepositor(request.getDepositor());

        remittanceRepository.save(originData);
    }

    public void delRemittance(long id ){
        remittanceRepository.deleteById(id);}}

package com.js.totossapi.service;


import com.js.totossapi.entity.Member;
import com.js.totossapi.model.member.MemberChangeRequest;
import com.js.totossapi.model.member.MemberItem;
import com.js.totossapi.model.member.MemberRequest;
import com.js.totossapi.model.member.MemberResponse;
import com.js.totossapi.resistory.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long memberId){return memberRepository.findById(memberId).orElseThrow();}

    public void setMember(MemberRequest request){
        Member addData = new Member();

        addData.setName(request.getName());
        addData.setBankName(request.getBankName());
        addData.setAccountNumber(request.getAccountNumber());

        memberRepository.save(addData);

    }
    public List<MemberItem> getMembers(){
        List<Member> originlist = memberRepository.findAll();
        List<MemberItem> result= new LinkedList<>();

        for(Member member : originlist){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setBankName(member.getBankName().getName());
            addItem.setAccountNumber(member.getAccountNumber());

            result.add(addItem);
        }return result;
    }
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBankName(originData.getBankName().getName());
        response.setAccountNumber(originData.getAccountNumber());

        return response;
    }
    public void putMember(long id , MemberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();

        originData.setName(request.getName());
        originData.setBankName(request.getBankName());
        originData.setAccountNumber(request.getAccountNumber());

        memberRepository.save(originData);
    }
}

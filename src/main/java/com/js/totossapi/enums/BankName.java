package com.js.totossapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BankName {
    KB("국민 은행")
    ,WOORI("우리 은행")
    ,SHINHAN("신한 은행")
    ,IBK("기업 은행");

    private final String name;
}

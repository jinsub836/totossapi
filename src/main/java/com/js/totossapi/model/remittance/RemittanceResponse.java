package com.js.totossapi.model.remittance;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RemittanceResponse {
    private Long id;

    private Long memberId;

    private String memberName;

    private String depositor;

    private String accountNumber;
}

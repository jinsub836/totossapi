package com.js.totossapi.model.remittance;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RemittaceChangeRequest {
    private String depositor;
}

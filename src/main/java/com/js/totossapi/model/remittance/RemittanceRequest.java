package com.js.totossapi.model.remittance;

import com.js.totossapi.enums.BankName;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RemittanceRequest {


    private String depositor;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private BankName bankName;

    private String accountNumber;

}

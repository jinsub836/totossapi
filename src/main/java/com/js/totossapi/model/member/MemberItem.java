package com.js.totossapi.model.member;

import com.js.totossapi.enums.BankName;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private Long id;

    private String name;

    private String bankName;

    private String accountNumber;
}

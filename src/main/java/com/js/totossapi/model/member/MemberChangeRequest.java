package com.js.totossapi.model.member;

import com.js.totossapi.enums.BankName;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeRequest {

    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private BankName bankName;

    private String accountNumber;
}

package com.js.totossapi.controller;

import com.js.totossapi.entity.Member;
import com.js.totossapi.model.remittance.RemittaceChangeRequest;
import com.js.totossapi.model.remittance.RemittanceItem;
import com.js.totossapi.model.remittance.RemittanceRequest;
import com.js.totossapi.model.remittance.RemittanceResponse;
import com.js.totossapi.service.MemberService;
import com.js.totossapi.service.RemittanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/remittance")
public class RemittanceController {
    private final RemittanceService remittanceService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    public String setRemittance(@RequestBody RemittanceRequest request, @PathVariable long memberId){
        Member member = memberService.getData(memberId);
        remittanceService.setRemittance(request,member);
        return "입력 완료";
    }

    @GetMapping("/all")
    public List<RemittanceItem>  getRemittances(){
        return remittanceService.getRemittances();}

    @GetMapping("/detail/{id}")
    public RemittanceResponse getRemittance(@PathVariable long id){
        return remittanceService.getRemittance(id);
    }

    @PutMapping("/change/{id}")
    public String putRemittance (@PathVariable long id , @RequestBody RemittaceChangeRequest request){
        remittanceService.putRemittance(id, request);
        return "수정 완료!!";
    }
    @DeleteMapping("/{id}")
    public String delRemittance (@PathVariable long id){
        remittanceService.delRemittance(id);
        return "삭제 완료";
    }
}

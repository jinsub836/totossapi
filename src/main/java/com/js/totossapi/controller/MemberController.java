package com.js.totossapi.controller;

import com.js.totossapi.model.member.MemberChangeRequest;
import com.js.totossapi.model.member.MemberItem;
import com.js.totossapi.model.member.MemberRequest;
import com.js.totossapi.model.member.MemberResponse;
import com.js.totossapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/member")
    public String setMember(MemberRequest request){
        memberService.setMember(request);
        return "등록 완료";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){
        return  memberService.getMembers();}

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return  memberService.getMember(id);
    }

    @PutMapping("/change/{id}")
    public String putMember (@PathVariable long id ,@RequestBody MemberChangeRequest request){
        memberService.putMember(id, request);
        return "수정 완료";
    }
}

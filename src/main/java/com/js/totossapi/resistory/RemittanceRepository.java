package com.js.totossapi.resistory;

import com.js.totossapi.entity.Remittance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RemittanceRepository extends JpaRepository<Remittance,Long> {
}

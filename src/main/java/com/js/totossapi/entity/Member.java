package com.js.totossapi.entity;

import com.js.totossapi.enums.BankName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false , length = 20)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private BankName bankName;

    @Column(nullable = false)
    private String accountNumber;
}

package com.js.totossapi.entity;

import com.js.totossapi.enums.BankName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Remittance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Member memberId;

    @Column(nullable = false)
    private LocalDate dateTrade;

    @Column(nullable = false , length = 20)
    private String depositor;

    @Column
    @Enumerated(value = EnumType.STRING)
    private BankName bankName;

    @Column(nullable = false, length = 9)
    private String accountNumber;}
